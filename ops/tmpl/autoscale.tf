variable "aws_region" {}
variable "aws_profile" {}

variable "project_name" {}
variable "project_env" {}
variable "cluster_name" {}
variable "service_name" {}
variable "ecs_service_autoscaling_role_arn" {}
variable "ecs_service_mem_high_thresold" { default=85 }
variable "ecs_service_mem_low_thresold" { default=50 }

provider "aws" {
  region = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

resource "aws_appautoscaling_target" "ecs_service" {
  max_capacity       = 5
  min_capacity       = 1
  resource_id        = "service/${var.cluster_name}/${var.service_name}"
  role_arn           = "${var.ecs_service_autoscaling_role_arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_service_scale_up" {
  adjustment_type         = "ChangeInCapacity"
  cooldown                = 900
  metric_aggregation_type = "Maximum"
  name                    = "ausp-${var.project_name}-${var.project_env}-ecs-${var.service_name}-scale-up"
  resource_id             = "service/${var.cluster_name}/${var.service_name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_adjustment {
    metric_interval_lower_bound = 0
    scaling_adjustment          = 1
  }

  depends_on = ["aws_appautoscaling_target.ecs_service"]
}

resource "aws_appautoscaling_policy" "ecs_service_scale_down" {
  adjustment_type         = "ChangeInCapacity"
  cooldown                = 900
  metric_aggregation_type = "Maximum"
  name                    = "ausp-${var.project_name}-${var.project_env}-ecs-${var.service_name}-scale-down"
  resource_id             = "service/${var.cluster_name}/${var.service_name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_adjustment {
    metric_interval_upper_bound = 0
    scaling_adjustment          = -1
  }

  depends_on = ["aws_appautoscaling_target.ecs_service"]
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_mem_high" {
    alarm_name = "alrm-${var.project_name}-${var.project_env}-ecs-${var.service_name}-mem-high"
    alarm_description = "This alarm monitors high memory of ecs service ${var.service_name}."
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "3"
    statistic = "Average"
    threshold = "${var.ecs_service_mem_high_thresold}"
    period = "300"
    metric_name = "MemoryUtilization"
    namespace = "AWS/ECS"
    dimensions {
      ClusterName = "${var.cluster_name}"
      ServiceName = "${var.service_name}"
    }
    alarm_actions = ["${aws_appautoscaling_policy.ecs_service_scale_up.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_mem_low" {
    alarm_name = "alrm-${var.project_name}-${var.project_env}-ecs-${var.service_name}-mem-low"
    alarm_description = "This alarm monitors low memory of ecs service ${var.service_name}."
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "3"
    statistic = "Average"
    threshold = "${var.ecs_service_mem_low_thresold}"
    period = "300"
    metric_name = "MemoryUtilization"
    namespace = "AWS/ECS"
    dimensions {
      ClusterName = "${var.cluster_name}"
      ServiceName = "${var.service_name}"
    }
    alarm_actions = ["${aws_appautoscaling_policy.ecs_service_scale_down.arn}"]
}
