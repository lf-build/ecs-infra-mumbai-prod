
whitelisted_cidr_list = ["111.93.247.122/32"]

# Ubuntu 14.04
# To find ami id
# search for ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-20170405
# under community AMI in that aws region.
base_amis = {
  us-east-1 = "ami-772aa961"
  ap-south-1 = "ami-1c200c73"
}
