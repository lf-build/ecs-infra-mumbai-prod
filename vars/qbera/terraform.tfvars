az_count = 2

ecs_ansible_s3_region = "ap-south-1" # this scripts are for installation of docker,fluentd,sysdig etc which is common across all the regions, hence no need to specify the region
ecs_ansible_s3_bucket = "ce-src-apsouth1" # this bucket should not be deleted
ecs_ansible_s3_prefix = "/ecs-ansible" # it should be empty or should start with /
