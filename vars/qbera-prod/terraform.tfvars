
instance_type = "r5d.xlarge"
asg_min = 1
asg_desired = 1
asg_max = 1

root_disk_size = 150
alrm_ecs_memr_thresold = 90
asg_memr_high_thresold = 85

user_data_type = "custom1"

http_enabled = false
https_enabled = true
lbsg_http_enabled = false
lbsg_https_enabled = true
#ssl_cert_arn = "arn:aws:acm:ap-southeast-1:993978665313:certificate/d40e2186-8e93-43d3-88c1-7a877f468347"
ssl_cert_arn = "arn:aws:acm:ap-south-1:993978665313:certificate/5d493f48-41cd-459d-a6eb-0824a6e82d2e" 
