#!/bin/bash

scriptdir=`dirname $0`
scriptname=`basename $0`
cd $scriptdir

project_name=$1
shift
project_env=$1
shift
cmd=$1
shift

display_usage() {
  echo -e "\nUsage:\n${scriptname} <project-name> <project-env> <cmd>\n"
}

[[ -z ${project_name} || -z ${project_env} || -z ${cmd} ]] && display_usage && exit 1

export PROJECT_NAME=$project_name
export PROJECT_ENV=$project_env
export PROJECT=${PROJECT_NAME}-${PROJECT_ENV}

[[ -f vars/main.vars ]] && source vars/main.vars
[[ -f vars/${PROJECT_NAME}/main.vars ]] && source vars/${PROJECT_NAME}/main.vars
[[ -f vars/${PROJECT}/main.vars ]] && source vars/${PROJECT}/main.vars
[[ -f vars/local.vars ]] && source vars/local.vars

[[ -z $ES_INDEX_PREFIX ]] && export ES_INDEX_PREFIX="${PROJECT}-"
[[ -z $ES2CSV_PATH ]] && export ES2CSV_PATH=es2csv
[[ -z $ESQL_PATH ]] && export ESQL_PATH=.build/esql

[[ -z $ES_EXPORT_PATH ]] && export ES_EXPORT_PATH=logs
[[ -z $ES_EXPORT_SHARE_PATH ]] && export ES_EXPORT_SHARE_PATH=${ES_EXPORT_PATH}/share
[[ -z $ES_EXPORT_ARCHIVE_PATH ]] && export ES_EXPORT_ARCHIVE_PATH=${ES_EXPORT_PATH}/archive

function _check_elktail {
  mkdir -p .build

  if [[ -z $ELKTAIL_PATH ]]; then
    ELKTAIL_PATH=elktail
    command -v elktail >/dev/null 2>&1 || ELKTAIL_PATH="./.build/elktail"
    # _download_elktail_if
  fi

  [[ ${ELKTAIL_PATH} != "elktail" && ! -x $ELKTAIL_PATH ]] && echo "elktail not found" && exit 1
}

function _elktail_init {
  [[ -z $ELKTAIL_AUTH_PASS ]] && export ELKTAIL_AUTH_PASS=${ES_PASS}
  [[ -z $ELKTAIL_INDEX_PATTERN ]] && ELKTAIL_INDEX_PATTERN="${ES_INDEX_PREFIX}[0-9].*"
  [[ -z $ELKTAIL_LOG_FORMAT ]] && ELKTAIL_LOG_FORMAT="%@timestamp|%source|%container_name|%log"

  ELKTAIL_OPTS=""
  [[ ! -z $ES_URL ]] && ELKTAIL_OPTS="$ELKTAIL_OPTS --url ${ES_URL}"
  [[ ! -z $ES_USER ]] && ELKTAIL_OPTS="$ELKTAIL_OPTS -u ${ES_USER}"
  [[ ! -z $ELKTAIL_INDEX_PATTERN ]] && ELKTAIL_OPTS="$ELKTAIL_OPTS -i '${ELKTAIL_INDEX_PATTERN}'"
  [[ ! -z $ELKTAIL_LOG_FORMAT ]] && ELKTAIL_OPTS="$ELKTAIL_OPTS --format ${ELKTAIL_LOG_FORMAT}"

  _check_elktail
}

function run_curator_cli {
  [[ -z ${ES_CURATOR_BIN_LOC} ]] && ES_CURATOR_CLI=curator_cli
  [[ ! -z ${ES_CURATOR_BIN_LOC} ]] && ES_CURATOR_CLI=$ES_CURATOR_BIN_LOC/curator_cli
  $ES_CURATOR_CLI --use_ssl --host "${ES_HOST}" --port "${ES_PORT}" --http_auth "${ES_USER}:${ES_PASS}" $@
}

function do_you_want_to_continue {
  choice=""
  read -p "$1 [ yes(y) / no(n) / exit(x) ]? " choice
  [[ $choice == "x" ]] && exit
}

function parse_date {
  arg1=$1
  rtndate=""
  format="%Y-%m-%d"
  fullformat="${format}T00:00"

  case $arg1 in
    *h)
      [[ $OSTYPE == linux* ]] && arg1="${arg1}our"
      [[ $OSTYPE == darwin* ]] && arg1="${arg1%?}H"
      fullformat="${format}T%H:%M"
      ;;
    *d)
      [[ $OSTYPE == linux* ]] && arg1="${arg1}ay"
      ;;
    *w)
      [[ $OSTYPE == linux* ]] && arg1="${arg1}eek"
      ;;
    *m)
      [[ $OSTYPE == linux* ]] && arg1="${arg1}onth"
      ;;
  esac

  [[ $OSTYPE == linux* ]] && rtndate=`date --date=-${arg1} -u +"${fullformat}"`
  [[ $OSTYPE == darwin* ]] && rtndate=`date -v-${arg1} -u +"${fullformat}"`
}

case ${cmd} in

tail | logs)
  _elktail_init

  [[ $ES_SCRIPT_DISPLAY_PASS == "y" ]] && echo $ES_PASS
  echo ${ELKTAIL_PATH} $ELKTAIL_OPTS $@
  ${ELKTAIL_PATH} $ELKTAIL_OPTS $@
  ;;

indexes)
  prefix=$1
  shift
  filter_list="{\"filtertype\":\"pattern\",\"kind\":\"prefix\",\"value\":\"${ES_INDEX_PREFIX}\"}"
  [[ $prefix == "all" ]] && filter_list='{"filtertype":"none"}'
  ES_CURATOR_CLI_OPTS="show_indices --verbose --filter_list $filter_list"
  [[ ! -z $SHOW_HEADER ]] && ES_CURATOR_CLI_OPTS="$ES_CURATOR_CLI_OPTS --header"
  run_curator_cli $ES_CURATOR_CLI_OPTS
  ;;

archive)
  size=$1
  shift
  [[ -z $size ]] && size=60
  mkdir -p $ES_EXPORT_PATH
  filter_list="[{\"filtertype\":\"pattern\",\"kind\":\"prefix\",\"value\":\"${ES_INDEX_PREFIX}\"},{\"filtertype\":\"space\",\"disk_space\":\"$size\"}]"
  ES_CURATOR_CLI_OPTS="--dry-run open --filter_list $filter_list"

  echo "==> Started at `date`"
  echo "==> Listing indexes to be archived"
  indexlist=`run_curator_cli $ES_CURATOR_CLI_OPTS`
  for index in `echo $indexlist`
  do
    [[ $index == ${ES_INDEX_PREFIX}* ]] && echo "    $index"
  done

  for index in `echo $indexlist`
  do
    if [[ $index == ${ES_INDEX_PREFIX}* ]]; then
      choice=""
      if [[ -z $ES_INTERATIVE || $ES_INTERATIVE == 'y' ]]; then
        while [ -z $choice ]; do
          do_you_want_to_continue "==> Archiving $index"
        done
      else
        echo "==> Archiving $index"
      fi
      if [[ $choice == 'n' ]]; then
        continue;
      fi
      # dump the index
      echo ">>> 1. Exporting $index"
      exportok=0
      tarok=0
      logfilename=${index}.csv
      logfilepath=$ES_EXPORT_PATH/${logfilename}
      tarfilepath=$ES_EXPORT_ARCHIVE_PATH/${logfilename}.tar.gz
      echo ">>>    Log File: $logfilepath"
      echo ">>>    Archive File: $tarfilepath"
      [[ ! -z ${FORCE_EXPORT} && ( -f $logfilepath || -f $tarfilepath ) ]] && rm -rf $logfilepath $tarfilepath
      [[ -f $logfilepath ]] && echo ">>>    Exported file found."
      if [[ ! -f $logfilepath ]]; then
        exportok=1
        $ES2CSV_PATH --verify-certs -u $ES_URL -a $ES_HTTP_AUTH -i "${index}" -q '*' -s 1000 -d '|' -o $logfilepath
        exportok=$?
      fi
      if [[ -f $logfilepath ]]; then
        [[ -f $tarfilepath ]] && echo ">>>    Tar-ed file found."
        if [[ ! -f $tarfilepath ]]; then
          tarok=1
          echo ">>>    Tar-ing exported file for archive."
          mkdir -p $ES_EXPORT_ARCHIVE_PATH
          tar -zcf $tarfilepath -C $ES_EXPORT_PATH $logfilename
          tarok=$?
        fi
      fi
      # move the file to s3
      echo ">>> 2. Syncing to s3"
      aws --profile ${AWS_PROFILE} --region ${ES_EXPORT_ARCHIVE_S3_REGION} s3 sync $ES_EXPORT_ARCHIVE_PATH/ s3://${ES_EXPORT_ARCHIVE_S3_BUCKET}/${PROJECT}-archive/
      syncok=$?
      # delete index
      echo "$exportok $tarok $syncok"
      if [[ $exportok == 0 && $tarok == 0 && $syncok == 0 ]]; then
        echo ">>> 3. Deleting $index"
        [[ -z $DRYRUN || $DRYRUN == 'y' ]] && CURATOR_OPTS="--dry-run"
        run_curator_cli ${CURATOR_OPTS} delete_indices --filter_list "{\"filtertype\":\"pattern\",\"kind\":\"prefix\",\"value\":\"${index}\"}"
        if [[ $DRYRUN == 'n' && $ES_RM_LOCAL_LOGFILE == 'y' ]]; then
          echo ">>> 4. Deleting $logfilepath"
          rm -rf $logfilepath
        fi
        if [[ $DRYRUN == 'n' && $ES_RM_LOCAL_TARFILE == 'y' ]]; then
          echo ">>> 5. Deleting $tarfilepath"
          rm -rf $tarfilepath
        fi
      fi
    fi
  done
  ;;

delete-index | rm-index)
  index=$1
  [[ -z $index ]] && echo "Error: arg index missing" && exit 1
  [[ -z $DRYRUN || $DRYRUN == 'y' ]] && CURATOR_OPTS="--dry-run"
  run_curator_cli ${CURATOR_OPTS} delete_indices --filter_list "{\"filtertype\":\"pattern\",\"kind\":\"prefix\",\"value\":\"${index}\"}"
  ;;

cleanup)
  [[ -z ${ES_CURATOR_BIN_LOC} ]] && ES_CURATOR=curator
  [[ ! -z ${ES_CURATOR_BIN_LOC} ]] && ES_CURATOR=$ES_CURATOR_BIN_LOC/curator
  ES_CURATOR_OPTS="--config ops/tmpl/es-curator.yml"
  [[ -z ${RUN_CURATOR} ]] && ES_CURATOR_OPTS="${ES_CURATOR_OPTS} --dry-run"
  echo "${ES_CURATOR} ${ES_CURATOR_OPTS} ops/tmpl/es-cleanup.yaml"
  ${ES_CURATOR} ${ES_CURATOR_OPTS} ops/tmpl/es-cleanup.yaml
  ;;

export)
  name=$1
  shift
  from=$1
  shift
  to=$1
  shift

  [[ -z $name ]] && echo "name arg missing" && exit 1 # todo check service
  [[ -z $from ]] && echo "fromdate arg missing - supported syntax n[mwdh] e.g. 1d 2w" && exit 1
  mkdir -p $ES_EXPORT_PATH

  # check format
  if [[ $from =~ ^[0-9]{1,2}[mwdh]$ ]]; then
    from_date_format="simple"
  else
    if [[ $OSTYPE == linux* ]]; then
      date "+%Y-%m-%d" -d "$from" > /dev/null 2>&1
      [[ $? == 0 ]] && from_date_format="ymd"
    fi
    if [[ $OSTYPE == darwin* ]]; then
      date -jf "%Y-%m-%d" "$from" > /dev/null 2>&1
      [[ $? == 0 ]] && from_date_format="ymd"
    fi
    [[ $from_date_format != "ymd" ]] && echo "fromdate arg invalid." && exit 1
  fi

  if [[ ! -z $to ]]; then
    if [[ $to =~ ^[0-9]{1,2}[mwdh]$ ]]; then
      to_date_format="simple"
    else
      if [[ $OSTYPE == linux* ]]; then
        date "+%Y-%m-%d" -d "$to" > /dev/null 2>&1
        [[ $? == 0 ]] && to_date_format="ymd"
      fi
      if [[ $OSTYPE == darwin* ]]; then
        date -jf "%Y-%m-%d" "$to" > /dev/null 2>&1
        [[ $? == 0 ]] && to_date_format="ymd"
      fi
      [[ $to_date_format != "ymd" ]] && echo "todate arg invalid." && exit 1
    fi
  fi

  # if simple format
  if [[ $from_date_format == "simple" ]]; then
    parse_date $from
    fromdate=$rtndate
    [[ -z $fromdate ]] && echo "issue formating from date! -- $from" && exit 1

    if [[ $to_date_format == "simple" ]]; then
      parse_date $to
      todate=$rtndate
      [[ -z $todate ]] && echo "issue formating to date! -- $to" && exit 1
    else
      toformat="${format}T23:59"
      todate=`date -u +"${toformat}"`
    fi
  fi

  # if in date format
  if [[ $from_date_format == "ymd" ]]; then
    fromdate="${from}T00:00"
    todate="${from}T23:59"
    [[ $to_date_format == "ymd" ]] && todate="${to}T23:59"
  fi

  case $name in
    -)
      _stdin=$(</dev/stdin)
      query="AND ${_stdin}"
      logprefix=${_stdin//[!a-zA-Z0-9]/_}
      ;;
    *=*)
      query="AND $name"
      logprefix=${name//[!a-zA-Z0-9]/_}
      ;;
    all)
      query=""
      logprefix=${name}
      ;;
    *)
      query="AND container_name = '$name'"
      logprefix=${name}
      ;;
  esac

  id=`cat /dev/urandom | tr -dc 'a-z0-9' | head -c 16`
  queryfile=/tmp/query_${id}.sql
  logfilename=${logprefix}_${fromdate/:/-}_${todate/:/-}_${id}.log
  logfilepath=$ES_EXPORT_PATH/${logfilename}
  tarfilepath=$ES_EXPORT_SHARE_PATH/${logfilename}.tar.gz
  [[ -z $ES_EXPORT_FIELDS ]] && export ES_EXPORT_FIELDS="@timestamp source container_name log"
  [[ -z $ES_EXPORT_LOGFILE_DO ]] && ES_EXPORT_LOGFILE_DO="rm"
  rm -rf *.tmp
  echo "SELECT * FROM x WHERE @timestamp between '$fromdate' AND '$todate' $query " | $ESQL_PATH | jq '{"query" : .query}' > $queryfile
  [[ ! -z $DEBUG || ! -z $DRYRUN ]] && cat $queryfile
  if [[ -z $DRYRUN ]]; then
    $ES2CSV_PATH --verify-certs -u $ES_URL -a $ES_HTTP_AUTH -i "${ES_INDEX_PREFIX}*" -s 1000 -r -q @$queryfile -f ${ES_EXPORT_FIELDS} -d '|' -o $logfilepath
    if [[ -f $logfilepath ]]; then
      mkdir -p $ES_EXPORT_SHARE_PATH
      tar -zcf $tarfilepath -C $ES_EXPORT_PATH $logfilename
    fi
  fi
  [[ $ES_EXPORT_LOGFILE_DO == "rm" ]] && rm -rf $logfilepath
  rm -rf $queryfile
  ;;

sync2s3)
  aws --profile ${AWS_PROFILE} --region ${ES_EXPORT_SHARE_S3_REGION} s3 sync $ES_EXPORT_SHARE_PATH/ s3://${ES_EXPORT_SHARE_S3_BUCKET}/${PROJECT}-logs/ --acl public-read
  ;;

*)
  display_usage
  ;;

esac
