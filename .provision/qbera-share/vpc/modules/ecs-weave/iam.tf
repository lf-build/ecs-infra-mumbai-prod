## IAM

resource "aws_iam_instance_profile" "nodes" {
  name  = "iprofile-${var.project_name}-${var.project_env}-nodes-mumbai"
  roles = ["${aws_iam_role.nodes.name}"]
}

resource "aws_iam_role" "nodes" {
  name = "role-${var.project_name}-${var.project_env}-nodes-mumbai"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "template_file" "nodes_policy_json" {
  template = "${file("${path.module}/files/nodes-policy.json")}"
  vars {
    ecs_ansible_s3_bucket = "${var.ecs_ansible_s3_bucket}"
    ecs_ansible_s3_prefix = "${var.ecs_ansible_s3_prefix}"
    fs_s3_bucket = "${var.fs_s3_bucket}"
    fs_s3_prefix = "${var.fs_s3_prefix}"
  }
}

resource "aws_iam_role_policy" "nodes" {
  name   = "rpolicy-${var.project_name}-${var.project_env}-nodes"
  role   = "${aws_iam_role.nodes.name}"
  policy = "${data.template_file.nodes_policy_json.rendered}"
}

resource "aws_iam_role" "ecs_service" {
  name = "role-${var.project_name}-${var.project_env}-ecs-service-mumbai"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "template_file" "ecs_service_policy_json" {
  template = "${file("${path.module}/files/ecs_service-policy.json")}"
}

resource "aws_iam_role_policy" "ecs_service" {
  name = "rpolicy-${var.project_name}-${var.project_env}-ecs-service"
  role = "${aws_iam_role.ecs_service.name}"
  policy = "${data.template_file.ecs_service_policy_json.rendered}"
}

resource "aws_iam_role" "ecs_service_autoscaling" {
  name = "role-${var.project_name}-${var.project_env}-ecs-service-autoscaling-mumbai"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "application-autoscaling.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "template_file" "ecs_service_autoscaling_policy_json" {
  template = "${file("${path.module}/files/ecs_service_autoscaling-policy.json")}"
}

resource "aws_iam_role_policy" "ecs_service_autoscaling" {
  name = "rpolicy-${var.project_name}-${var.project_env}-ecs-service-autoscaling"
  role = "${aws_iam_role.ecs_service_autoscaling.name}"
  policy = "${data.template_file.ecs_service_autoscaling_policy_json.rendered}"
}
