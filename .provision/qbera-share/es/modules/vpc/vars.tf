variable "project_name" {
  description = "The project name."
}

variable "project_env" {
  description = "The project env."
}

variable "aws_region" {
  description = "The AWS region to create things in."
}

variable "vpc_cidr" {
  description = "The AWS VPC CIDR."
}

variable "aws_profile" {
  description = "The AWS credentials profile name."
}

variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
}

variable "vpc_peering_route_enabled" {
  description = "Boolean: need to route vpc peering?"
  default = false
}

variable "vpc_peering_id" {
  description = "vpc peering id"
  default = ""
}
