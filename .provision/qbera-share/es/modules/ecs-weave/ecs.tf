## ECS

resource "aws_ecs_cluster" "main" {
  name = "${var.project_name}-${var.project_env}"
}

resource "aws_cloudwatch_log_group" "main" {
  name = "${aws_ecs_cluster.main.name}"
  retention_in_days = "${var.log_retention_in_days}"
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_autoscaling_policy" "nodes_scaleup" {
    name = "asp-${var.project_name}-${var.project_env}-nodes-scaleup"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.nodes.name}"
}

resource "aws_autoscaling_policy" "nodes_scaledown" {
    name = "asp-${var.project_name}-${var.project_env}-nodes-scaledown"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.nodes.name}"
}

resource "aws_cloudwatch_metric_alarm" "nodes_mem_high" {
    alarm_name = "alrm-${var.project_name}-${var.project_env}-nodes-memr-high"
    alarm_description = "This alarm monitors high memr of ecs cluster."
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    statistic = "Average"
    threshold = "${var.asg_memr_high_thresold}"
    period = "300"
    metric_name = "MemoryReservation"
    namespace = "AWS/ECS"
    dimensions {
        ClusterName = "${aws_ecs_cluster.main.name}"
    }
    alarm_actions = ["${aws_autoscaling_policy.nodes_scaleup.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "nodes_mem_low" {
    alarm_name = "alrm-${var.project_name}-${var.project_env}-nodes-memr-low"
    alarm_description = "This alarm monitors low memr of ecs cluster."
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    statistic = "Average"
    threshold = "${var.asg_memr_low_thresold}"
    period = "300"
    metric_name = "MemoryReservation"
    namespace = "AWS/ECS"
    dimensions {
        ClusterName = "${aws_ecs_cluster.main.name}"
    }
    alarm_actions = ["${aws_autoscaling_policy.nodes_scaledown.arn}"]
}
