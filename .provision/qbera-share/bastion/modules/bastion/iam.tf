

resource "aws_iam_role" "bastion" {
  name = "role-${var.project_name}-${var.project_env}-bastion-mumbai"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "template_file" "bastion_policy_json" {
  template = "${file("${path.module}/files/bastion-policy.json")}"
}

resource "aws_iam_role_policy" "bastion" {
  name   = "rpolicy-${var.project_name}-${var.project_env}-bastion"
  role   = "${aws_iam_role.bastion.name}"
  policy = "${data.template_file.bastion_policy_json.rendered}"
}

resource "aws_iam_instance_profile" "bastion" {
  name  = "iprofile-${var.project_name}-${var.project_env}-bastion-mumbai"
  roles = ["${aws_iam_role.bastion.name}"]
}
