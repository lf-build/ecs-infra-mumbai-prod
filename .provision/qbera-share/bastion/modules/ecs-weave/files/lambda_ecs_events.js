'use strict';
var AWS = require("aws-sdk");

exports.handler = (event, context, callback) => {
  callback(null, 'Finished');

  var publish = true;
  var subject = "Alert";

  var eventText = JSON.stringify(event, null, 2);
  if (process.env.DEBUG) {
    console.log("Received event:", eventText);
  }

  if (event["detail-type"] && event["detail-type"] == "ECS Task State Change") {
    var detail = event.detail;
    var container = detail.containers[0];
    var clustername = detail.clusterArn.split("/")[1];
    var taskdef = detail.taskDefinitionArn.split("/")[1];
    publish = (
      //(container.lastStatus == "STOPPED" && detail.desiredStatus == "RUNNING" && detail.lastStatus == "RUNNING") || // crash stop scenario
      (container.lastStatus == "STOPPED" && detail.desiredStatus == "STOPPED" && detail.lastStatus == "STOPPED") || // manual stop scenario
      (container.lastStatus == "RUNNING" && detail.desiredStatus == "RUNNING" && detail.lastStatus == "RUNNING") // start scenario
    );
    subject = "Alert/ECS/" + clustername + "/" + taskdef + "/" + container.lastStatus;
  }

  if (publish) {
    var sns = new AWS.SNS();
    var params = {
      Message: eventText,
      Subject: subject,
      TopicArn: process.env.TOPIC_ARN
    };

    sns.publish(params, context.done);
  }
};
