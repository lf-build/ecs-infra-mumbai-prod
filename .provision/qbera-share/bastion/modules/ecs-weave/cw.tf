
resource "aws_sns_topic" "alarm" {
  name = "sns-${var.project_name}-${var.project_env}-alarm"
}

/*
resource "aws_sns_topic_subscription" "alarm" {
  topic_arn = "${aws_sns_topic.alarm.arn}"
  protocol  = "lambda"
  endpoint  = "${var.alarm_lambda_arn}"
}
*/

resource "aws_iam_role" "lambda_ecs_events" {
  name = "role-${var.project_name}-${var.project_env}-lambda_ecs_events-mumbai"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "template_file" "lambda_ecs_events_policy" {
  template = "${file("${path.module}/files/lambda-policy.json")}"
}

resource "aws_iam_role_policy" "lambda_ecs_events" {
  name   = "rpolicy-${var.project_name}-${var.project_env}-lambda_ecs_events"
  role   = "${aws_iam_role.lambda_ecs_events.name}"
  policy = "${data.template_file.lambda_ecs_events_policy.rendered}"
}

data "archive_file" "lambda_ecs_events_zip" {
  type        = "zip"
  source_file  = "${path.module}/files/lambda_ecs_events.js"
  output_path = "lambda-ecs-events.zip"
}

resource "aws_lambda_function" "lambda_ecs_events" {
  filename         = "lambda-ecs-events.zip"
  source_code_hash = "${data.archive_file.lambda_ecs_events_zip.output_base64sha256}"
  function_name    = "mumbai-lambda_${var.project_name}_${var.project_env}_ecs_events"
  role             = "${aws_iam_role.lambda_ecs_events.arn}"
  handler          = "lambda_ecs_events.handler"
  runtime          = "nodejs8.10"
  description      = "Lambda function to send alert/event notification to SNS."

  environment {
    variables = {
      TOPIC_ARN = "${aws_sns_topic.alarm.arn}"
    }
  }
}

resource "aws_cloudwatch_event_rule" "lambda_ecs_events" {
  name   = "cwrule-${var.project_name}-${var.project_env}-lambda_ecs_events-mumbai"
  description = ""

  event_pattern = <<PATTERN
{
  "source": [
    "aws.ecs"
  ],
  "detail-type": [
    "ECS Task State Change"
  ],
  "detail": {
    "clusterArn": [
      "${aws_ecs_cluster.main.id}"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "lambda_ecs_events" {
  target_id = "cwtarget-${var.project_name}-${var.project_env}-lambda_ecs_events"
  rule      = "${aws_cloudwatch_event_rule.lambda_ecs_events.name}"
  arn       = "${aws_lambda_function.lambda_ecs_events.arn}"
}

resource "aws_lambda_permission" "lambda_ecs_events" {
  statement_id = "lp-${var.project_name}-${var.project_env}-lambda_ecs_events"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_ecs_events.arn}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.lambda_ecs_events.arn}"
}

resource "aws_cloudwatch_metric_alarm" "ecs_cpu" {
  alarm_name                = "alrm-${var.project_name}-${var.project_env}-ecs-CPUUtilization"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/ECS"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = "80"
  alarm_description         = "This alarm monitor ecs cluster cpu utilization"
  insufficient_data_actions = []
  dimensions {
    ClusterName = "${aws_ecs_cluster.main.name}"
  }
  alarm_actions             = ["${aws_sns_topic.alarm.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "ecs_mem" {
  alarm_name                = "alrm-${var.project_name}-${var.project_env}-ecs-MemoryUtilization"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "MemoryUtilization"
  namespace                 = "AWS/ECS"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = "80"
  alarm_description         = "This alarm monitor ecs cluster memory utilization"
  insufficient_data_actions = []
  dimensions {
    ClusterName = "${aws_ecs_cluster.main.name}"
  }
  alarm_actions             = ["${aws_sns_topic.alarm.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "ecs_mem_res" {
  alarm_name                = "alrm-${var.project_name}-${var.project_env}-ecs-MemoryReservation"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "MemoryReservation"
  namespace                 = "AWS/ECS"
  period                    = "300"
  statistic                 = "Average"
  threshold                 = "${var.alrm_ecs_memr_thresold}"
  alarm_description         = "This alarm monitor ecs cluster memory reservation"
  insufficient_data_actions = []
  dimensions {
    ClusterName = "${aws_ecs_cluster.main.name}"
  }
  alarm_actions             = ["${aws_sns_topic.alarm.arn}"]
}
