

whitelisted_cidr_list = ["111.93.247.122/32"]

# Ubuntu 14.04
# To find ami id
# search for ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-20170405
# under community AMI in that aws region.
base_amis = {
  us-east-1 = "ami-772aa961"
  ap-south-1 = "ami-1c200c73"
}
az_count = 2

ecs_ansible_s3_region = "ap-south-1" # this scripts are for installation of docker,fluentd,sysdig etc which is common across all the regions, hence no need to specify the region
ecs_ansible_s3_bucket = "ce-src-apsouth1" # this bucket should not be deleted
ecs_ansible_s3_prefix = "/ecs-ansible" # it should be empty or should start with /

instance_type = "t2.micro"

enable_bastion_disk1 = true
bastion_disk1_size = 50
