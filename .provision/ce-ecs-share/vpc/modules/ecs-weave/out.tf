
output "ecs_cluster_name" {
  value = "${aws_ecs_cluster.main.name}"
}

output "ecs_service_role_name" {
  value = "${aws_iam_role.ecs_service.name}"
}

output "ecs_service_role_arn" {
  value = "${aws_iam_role.ecs_service.arn}"
}

output "ecs_service_autoscaling_role_arn" {
  value = "${aws_iam_role.ecs_service_autoscaling.arn}"
}

output "ops_alb_id" {
  value = "${aws_alb.ops.id}"
}

output "ops_alb_hostname" {
  value = "${aws_alb.ops.dns_name}"
}

output "alb_tg_backoffice" {
  value = "${aws_alb_target_group.backoffice.id}"
}

output "alb_tg_bankportal" {
  value = "${aws_alb_target_group.bankportal.id}"
}

output "alb_tg_services" {
  value = "${aws_alb_target_group.services.id}"
}

#output "alb_ui_id" {
#  value = "${aws_alb.ui.id}"
#}

#output "alb_ui_hostname" {
#  value = "${aws_alb.ui.dns_name}"
#}

output "alb_tg_ui" {
  value = "${aws_alb_target_group.ui.id}"
}

output "alb_tg_ui_proxy" {
  value = "${aws_alb_target_group.ui_proxy.id}"
}

output "alb_tg_ui_maint" {
  value = "${aws_alb_target_group.ui_maint.id}"
}

#output "alb_app_id" {
#  value = "${aws_alb.app.id}"
#}

#output "alb_app_hostname" {
#  value = "${aws_alb.app.dns_name}"
#}

output "alb_tg_app" {
  value = "${aws_alb_target_group.app.id}"
}

#output "alb_orbit_id" {
#  value = "${aws_alb.orbit.id}"
#}

#output "alb_orbit_hostname" {
#  value = "${aws_alb.orbit.dns_name}"
#}

output "alb_tg_orbit" {
  value = "${aws_alb_target_group.orbit.id}"
}

#output "alb_verify_id" {
#  value = "${aws_alb.verify.id}"
#}

#output "alb_verify_hostname" {
#  value = "${aws_alb.app.dns_name}"
#}

output "alb_tg_verify" {
  value = "${aws_alb_target_group.verify.id}"
}

output "alb_tg_static" {
  value = "${aws_alb_target_group.static.id}"
}
