
data "aws_ami" "amazon_ami_latest" {
  most_recent = true

  filter {
    name = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name = "name"
    values = ["${var.amazon_ami_name}"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

resource "aws_instance" "bastion" {
    ami = "${data.aws_ami.amazon_ami_latest.id}"
    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"
    vpc_security_group_ids = ["${aws_security_group.bastion.id}"]
    subnet_id = "${var.vpc_public_subnet_id}"
    iam_instance_profile = "${aws_iam_instance_profile.bastion.name}"
    source_dest_check = false
    tags {
        Name = "ec2-${var.project_name}-${var.project_env}-bastion"
        Project = "${var.project_name}"
        ENV = "${var.project_env}"
        ManagedBy = "terraform"
    }
}

resource "aws_eip" "bastion" {
  instance = "${aws_instance.bastion.id}"
  vpc      = true
}

resource "aws_eip_association" "bastion" {
  instance_id = "${aws_instance.bastion.id}"
  allocation_id = "${aws_eip.bastion.id}"
}

resource "aws_ebs_volume" "bastion" {
    count = "${var.enable_bastion_disk1}"
    availability_zone = "${aws_instance.bastion.availability_zone}"
    size = "${var.bastion_disk1_size}"
    type = "gp2"
    tags {
      Name = "ec2-${var.project_name}-${var.project_env}-bastion-${var.bastion_disk1_size}g"
      Project = "${var.project_name}"
      ENV = "${var.project_env}"
      ManagedBy = "terraform"
    }
}

resource "aws_volume_attachment" "bastion" {
    count = "${var.enable_bastion_disk1}"
    device_name = "/dev/sdb"
    volume_id = "${aws_ebs_volume.bastion.id}"
    instance_id = "${aws_instance.bastion.id}"
}
