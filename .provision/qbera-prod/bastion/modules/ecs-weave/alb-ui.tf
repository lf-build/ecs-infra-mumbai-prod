
## UI
resource "aws_alb" "ui" {
  name            = "alb-${var.project_name}-${var.project_env}-ui"
  internal        = false
  subnets         = ["${var.vpc_public_subnet_ids}"]
  security_groups = ["${aws_security_group.ui_lb.id}"]
  idle_timeout    = 180
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "ui_proxy" {
  name     = "tg-${var.project_name}-${var.project_env}-ui-proxy"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    matcher = "301"
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "ui_http" {
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.ui_proxy.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "ui" {
  name     = "tg-${var.project_name}-${var.project_env}-ui"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    interval = 30
    timeout = 15
    healthy_threshold = 10
    unhealthy_threshold = 10
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "ui_maint" {
  name     = "tg-${var.project_name}-${var.project_env}-ui-maint"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    matcher = "307"
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "ui_maint" {
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "1111"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.ui_maint.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "ui_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.ui_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${coalesce(var.ui_maint_tg_arn, aws_alb_target_group.ui.id)}"
    type             = "forward"
  }
}

## workflow
resource "aws_alb" "app" {
  name            = "alb-${var.project_name}-${var.project_env}-app"
  internal        = false
  subnets         = ["${var.vpc_public_subnet_ids}"]
  security_groups = ["${aws_security_group.app_lb.id}"]
  idle_timeout    = 120
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "app" {
  name     = "tg-${var.project_name}-${var.project_env}-app"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    path = "/healthcheck"
    interval = 30
    timeout = 15
    healthy_threshold = 10
    unhealthy_threshold = 10
    matcher = 401
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "app_http" { # Testing purpose
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "82"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "app_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "5443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.app_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.id}"
    type             = "forward"
  }
}

## orbit-identity
resource "aws_alb" "orbit" {
  name            = "alb-${var.project_name}-${var.project_env}-orbit"
  internal        = false
  subnets         = ["${var.vpc_public_subnet_ids}"]
  security_groups = ["${aws_security_group.orbit_lb.id}"]
  idle_timeout    = 120
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "orbit" {
  name     = "tg-${var.project_name}-${var.project_env}-orbit"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "orbit_http" { # Testing purpose
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.orbit.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "orbit_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "2443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.orbit_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.orbit.id}"
    type             = "forward"
  }
}

## verification
resource "aws_alb" "verify" {
  name            = "alb-${var.project_name}-${var.project_env}-verify"
  internal        = false
  subnets         = ["${var.vpc_public_subnet_ids}"]
  security_groups = ["${aws_security_group.verify_lb.id}"]
  idle_timeout    = 60
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "verify" {
  name     = "tg-${var.project_name}-${var.project_env}-verify"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    interval = 30
    timeout = 15
    healthy_threshold = 10
    unhealthy_threshold = 10
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "verify_http" { # Testing purpose
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "81"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.verify.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "verify_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "3443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.verify_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.verify.id}"
    type             = "forward"
  }
}
