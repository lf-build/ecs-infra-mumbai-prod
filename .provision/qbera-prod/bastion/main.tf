variable "aws_region" {}
variable "aws_profile" {}

variable "project_name" {}
variable "project_env" {}

variable "terraform_state_s3_bucket" {}
variable "terraform_state_s3_key_vpc" {}

variable "key_name" {}
variable "instance_type" {}
variable "enable_bastion_disk1" { default = 0 }
variable "bastion_disk1_size" { default = 50 }

provider "aws" {
  region = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

data "terraform_remote_state" "vpc" {
    backend = "s3"
    config {
        bucket = "${var.terraform_state_s3_bucket}"
        key = "${var.terraform_state_s3_key_vpc}"
        region = "${var.aws_region}"
        profile = "${var.aws_profile}"
    }
}

module "bastion" {
  source = "./modules/bastion"

  project_name = "${var.project_name}"
  project_env = "${var.project_env}"

  key_name = "${var.key_name}"
  instance_type = "${var.instance_type}"

  vpc_id = "${data.terraform_remote_state.vpc.vpc_id}"
  vpc_public_subnet_id = "${data.terraform_remote_state.vpc.vpc_public_subnet_ids[0]}"

  enable_bastion_disk1 = "${var.enable_bastion_disk1}"
  bastion_disk1_size = "${var.bastion_disk1_size}"
}

output "bastion_ip" { value = "${module.bastion.bastion_ip}" }
output "bastion_key" { value = "${module.bastion.bastion_key}" }
