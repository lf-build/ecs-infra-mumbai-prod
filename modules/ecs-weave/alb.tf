## ALB

resource "aws_alb" "ops" {
  name            = "alb-${var.project_name}-${var.project_env}-ops"
  internal        = false
  subnets         = ["${var.vpc_public_subnet_ids}"]
  security_groups = ["${aws_security_group.ops_lb.id}"]
  idle_timeout    = 60
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "weavescope" {
  name     = "tg-${var.project_name}-${var.project_env}-weavescope"
  port     = 4040
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"
  stickiness {
    type = "lb_cookie"
  }
  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "weavescope_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "4040"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.weavescope.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "weavescope_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "4443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.weavescope_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.weavescope.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "services" {
  name     = "tg-${var.project_name}-${var.project_env}-services"
  port     = 9090
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    path = "/ping"
    port = 8080
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "services_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "9090"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.services.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "services_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "9443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.services_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.services.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "backoffice" {
  name     = "tg-${var.project_name}-${var.project_env}-backoffice"
  port     = 9002
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "backoffice_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.backoffice.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "backoffice_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "8443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.backoffice_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.backoffice.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "bankportal" {
  name     = "tg-${var.project_name}-${var.project_env}-bankportal"
  port     = 9002
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "bankportal_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "7070"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.bankportal.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "bankportal_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "7443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.bankportal_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.bankportal.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "static" {
  name     = "tg-${var.project_name}-${var.project_env}-static"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "static_http" {
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "6060"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.static.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "static_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "6443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.static_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.static.id}"
    type             = "forward"
  }
}

## UI
resource "aws_alb_target_group" "ui_proxy" {
  name     = "tg-${var.project_name}-${var.project_env}-ui-proxy"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    matcher = "301"
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "ui_http" {
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.ui_proxy.id}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "ui" {
  name     = "tg-${var.project_name}-${var.project_env}-ui"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    interval = 30
    timeout = 15
    healthy_threshold = 10
    unhealthy_threshold = 10
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_target_group" "ui_maint" {
  name     = "tg-${var.project_name}-${var.project_env}-ui-maint"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    matcher = "307"
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "ui_maint" {
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "1111"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.ui_maint.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "ui_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.ui_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${coalesce(var.ui_maint_tg_arn, aws_alb_target_group.ui.id)}"
    type             = "forward"
  }
}

## Workflow
resource "aws_alb_target_group" "app" {
  name     = "tg-${var.project_name}-${var.project_env}-app"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    path = "/healthcheck"
    interval = 30
    timeout = 15
    healthy_threshold = 10
    unhealthy_threshold = 10
    matcher = 401
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "app_http" { # Testing purpose
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "82"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "app_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "5443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.app_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.app.id}"
    type             = "forward"
  }
}

## orbit-identity
resource "aws_alb_target_group" "orbit" {
  name     = "tg-${var.project_name}-${var.project_env}-orbit"
  port     = 85
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "orbit_http" { # Testing purpose
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "85"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.orbit.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "orbit_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "2443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.orbit_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.orbit.id}"
    type             = "forward"
  }
}

## Verification
resource "aws_alb_target_group" "verify" {
  name     = "tg-${var.project_name}-${var.project_env}-verify"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    interval = 30
    timeout = 15
    healthy_threshold = 10
    unhealthy_threshold = 10
  }

  tags {
    Project = "${var.project_name}"
    ENV = "${var.project_env}"
    ManagedBy = "terraform"
  }
}

resource "aws_alb_listener" "verify_http" { # Testing purpose
  count             = "${var.http_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "81"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.verify.id}"
    type             = "forward"
  }
}

resource "aws_alb_listener" "verify_https" {
  count             = "${var.https_enabled}"
  load_balancer_arn = "${aws_alb.ops.id}"
  port              = "3443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2015-05"
  certificate_arn   = "${coalesce(var.verify_ssl_cert_arn, var.ssl_cert_arn)}"

  default_action {
    target_group_arn = "${aws_alb_target_group.verify.id}"
    type             = "forward"
  }
}
