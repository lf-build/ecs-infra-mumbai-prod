#!/bin/bash

scriptdir=`dirname $0`
scriptname=`basename $0`
cd $scriptdir

project_name=$1
shift
project_env=$1
shift

display_usage() {
  echo -e "\nUsage:\n${scriptname} <project-name> <project-env>\n"
}

[[ -z ${project_name} || -z ${project_env} ]] && display_usage && exit 1

function do_you_want_to_continue {
  choice=""
  read -p "==> $1 [yes(y)/no(n)/x(exit)]? " choice
  [[ $choice == "x" ]] && exit
}

do_you_want_to_continue "WARNING: This command will destroy resources. Please becareful before you proceed!"

do_you_want_to_continue "Update all ecs services desired count to 0"

if [[ $choice == "y" ]]; then
  NOCHECK_LOOP=y DELAY=1 ./ecs ${project_name} ${project_env} stop infra
  NOCHECK_LOOP=y DELAY=1 ./ecs ${project_name} ${project_env} stop ui
  NOCHECK_LOOP=y DELAY=1 ./ecs ${project_name} ${project_env} stop all
fi

do_you_want_to_continue "Check and remove all ecs services"

if [[ $choice == "y" ]]; then
  ./ecs ${project_name} ${project_env} check-rm infra
  ./ecs ${project_name} ${project_env} check-rm ui
  ./ecs ${project_name} ${project_env} check-rm all
fi

do_you_want_to_continue "Check ecs cluster ${project_name}-${project_env} and remove any pending services manually."

do_you_want_to_continue "Destroy ecs infrastructure"

if [[ $choice == "y" ]]; then
  ./tf ${project_name} ${project_env} ecs-weave destroy
fi

do_you_want_to_continue "Destroy bastion infrastructure"

if [[ $choice == "y" ]]; then
  ./tf ${project_name} share bastion destroy
fi

do_you_want_to_continue "Remove VPC peering manually"

do_you_want_to_continue "Destroy VPC Infrastructure"

if [[ $choice == "y" ]]; then
  ./tf ${project_name} share vpc destroy
fi

do_you_want_to_continue "Destroy All Repositories"

if [[ $choice == "y" ]]; then
  ./ecr ${project_name} ${project_env} rm infra
  ./ecr ${project_name} ${project_env} rm ui
  ./ecr ${project_name} ${project_env} rm all
fi

do_you_want_to_continue "Destroy All ES Logs"

if [[ $choice == "y" ]]; then
  ES_UNIT=years ES_UNIT_VALUE=5 ./ecr ${project_name} ${project_env} cleanup
fi
